#!/usr/bin/python3
from queue import PriorityQueue
from uuid import uuid4
from random import sample
from dataclasses import dataclass, field
from typing import Any


NUMBER_OF_STUDENTS = 50
NUMBER_OF_HOSPITALS = 8


@dataclass(order=True)
class PrioritizedItem:
    priority: int
    item: Any=field(compare=False)

    def __iter__(self):
        yield self.priority
        yield self.item


class StudentMap:

    def get(self, key):
        return next(filter(lambda x: x.key == key, self.map))

    def get_all_free(self):
        return filter(lambda x: x.is_free(), self.map)

    def get_all_taken(self):
        return filter(lambda x: not x.is_free(), self.map)

    def generate_priority_maps(self, hospital_map):
        for student in self.map:
            student.generate_priority_map(hospital_map)

    def __sizeof__(self):
        return len(self.map)

    def __len__(self):
        return len(self.map)

    def __iter__(self):
        return self.map.__iter__()

    def __init__(self, student_map=None):
        if student_map:
            if isinstance(student_map, list):
                self.map = student_map
            else:
                raise TypeError('student_map must of of type list')
        else:
            self.map = []


class HospitalMap:

    def full(self):
        return False not in map(lambda x: x.full(), self.map)

    def run(self):
        for hospital in self.map:
            hospital.propose()

    def __iter__(self):
        return self.map.__iter__()

    def __len__(self):
        return len(self.map)

    def __init__(self, hospital_map):
        self.map = hospital_map


class Student:

    def evaluate_proposal(self, hospital):
        if self.hospital:
            if self._preference_map[hospital.key] < self._preference_map[self.hospital.key]:
                self._withdraw(self.hospital)
                self._accept_proposal(hospital)
                return True
        else:
            self._accept_proposal(hospital)
            return True
        return False

    def generate_priority_map(self, hospital_map):
        priority_list = sample(range(1, len(hospital_map) + 1), len(hospital_map))
        for idx, student in enumerate(hospital_map):
            self._preference_map[student.key] = priority_list[idx]

    def _accept_proposal(self, hospital):
        self.hospital = hospital

    def _withdraw(self, hospital):
        hospital.withdraw_student(self)

    def is_free(self):
        return self.hospital is None

    def __init__(self):
        self.key = str(uuid4())
        self.hospital = None
        self._preference_map = {}


class Hospital:

    def propose(self, student=None):
        if not self.full():
            if student:
                next_best_student = student
            else:
                priority, student_key = self.preference_queue.get()
                next_best_student = self.student_map.get(student_key)
            accepted = next_best_student.evaluate_proposal(self)
            if accepted:
                self.accept_student(next_best_student)

    def accept_student(self, student):
        assert(self.available_slots > 0)
        self.accepted_students.append(student)
        self.available_slots = self.available_slots - 1

    def withdraw_student(self, student):
        priority = self._original_map[student.key]
        self.preference_queue.put(PrioritizedItem(priority, student.key))
        self.available_slots = self.available_slots + 1
        self.accepted_students.remove(student)
        assert(self.available_slots <= self.max_slots)

    def full(self):
        return not (self.available_slots > 0)

    def __init__(self, student_map, priority_map=None):
        if not isinstance(student_map, StudentMap):
            raise TypeError('student_map must of of type StudentMap')
        self.key = str(uuid4())
        self.student_map = student_map
        if priority_map:
            self._original_map = priority_map
        else:
            self._original_map = {}
            priority_list = sample(range(1, len(student_map) + 1), len(student_map))
            for idx, student in enumerate(self.student_map):
                self._original_map[student.key] = priority_list[idx]
        self.max_slots = 5
        self.available_slots = self.max_slots
        self.accepted_students = []
        self.preference_queue = PriorityQueue()
        for student_key, priority in self._original_map.items():
            self.preference_queue.put(PrioritizedItem(priority, student_key))


def main():
    students = [Student() for x in range(NUMBER_OF_STUDENTS)]
    student_map = StudentMap(students)
    hospitals = [Hospital(student_map) for x in range(NUMBER_OF_HOSPITALS)]
    hospital_map = HospitalMap(hospitals)
    student_map.generate_priority_maps(hospital_map)

    while not hospital_map.full():
        hospital_map.run()

    for hospital in hospital_map:
        print('Hospital %(id)s students: %(accepted_students)s' % {
            'id': hospital.key,
            'accepted_students': list(map(lambda x: x.key, hospital.accepted_students)),
        })


if __name__ == '__main__':
    main()
